name 'ping-exporter'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/ping-exporter@incoming.gitlab.com'
license 'Apache-2.0'
description 'Install and configure ping exporter'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/ping-exporter'
issues_url 'https://gitlab.com/chef-platform/ping-exporter/issues'
version '1.1.0'

supports 'centos', '>= 7.3'

depends 'ark'
depends 'cluster-search', '>= 1.3.0'

chef_version '>= 12.19'
