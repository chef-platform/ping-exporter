Changelog
=========

1.0.0
-----

- Initial version with Centos support, fetching a pre-compiled binary from
  https://gitlab.com/sre-tools/ping-exporter.
