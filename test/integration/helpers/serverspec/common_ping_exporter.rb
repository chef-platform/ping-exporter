#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

config_file = '/opt/ping_exporter/config.yml'
config = { 'targets' => %w[ping-exporter-centos_7] }

describe file(config_file) do
  it { should contain(config.to_yaml) }
end

cli_opts = {
  'config.path' => config_file,
  'dns.refresh' => '1m0s',
  'ping.interval' => '5s',
  'ping.timeout' => '4s',
  'web.listen-address' => '0.0.0.0:9427',
  'web.telemetry-path' => '/metrics'
}.map { |k, v| "#{' ' * 2}-#{k} #{v}" }.join(" \\\n")

describe file('/opt/ping_exporter/ping_exporter') do
  it { should be_executable }
end

describe file('/etc/systemd/system/ping-exporter.service') do
  it { should contain(cli_opts) }
end

describe service('ping-exporter.service') do
  it { should be_enabled }
  it { should be_running.under('systemd') }
end

describe port(9427) do
  it { should be_listening }
end
