#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

cookbook_name = 'ping-exporter'

# Ping exporter version
default[cookbook_name]['version'] = 'master'
version = node[cookbook_name]['version']

# Where to get the binary
default[cookbook_name]['mirror_base'] =
  'https://gitlab.com/sre-tools/ping-exporter/-/jobs/artifacts'
mirror_base = node[cookbook_name]['mirror_base']

default[cookbook_name]['mirror'] =
  "#{mirror_base}/#{version}/download?job=build-ping-exporter"

# Checksum of ping exporter zip file
default[cookbook_name]['checksum'] =
  '2a69917db090c4f855a217debec39fcc979b48ffdbd9932df24108f90053c139'

# Ark stuff (Installation)
default[cookbook_name]['prefix_root'] = '/opt' # base installation dir
default[cookbook_name]['prefix_home'] = '/opt' # where is link to install dir
default[cookbook_name]['prefix_bin'] = '/opt/bin' # where to link binaries
prefix_home = node[cookbook_name]['prefix_home']

# Ping exporter user and group
# Retrieved from prometheus-platform cookbook if defined
# otherwise root user is used
if node.attribute?('prometheus-platform')
  user = node['prometheus-platform']['user']
  group = node['prometheus-platform']['group']
end

default[cookbook_name]['user'] = user.nil? ? 'root' : user
default[cookbook_name]['group'] = group.nil? ? 'root' : group

# Ping exporter config file
default[cookbook_name]['config_file'] = 'config.yml'
config_file = node[cookbook_name]['config_file']
config_path = "#{prefix_home}/ping_exporter/#{config_file}"

# Role used by the search to find ping exporter targets
default[cookbook_name]['targets']['role'] = nil
# Ping exporter targets, deactivate search if not empty
default[cookbook_name]['targets']['hosts'] = []

# Ping exporter cli opts
default[cookbook_name]['cli_opts'] = {
  'config.path' => config_path,
  'dns.refresh' => '1m0s',
  'ping.interval' => '5s',
  'ping.timeout' => '4s',
  'web.listen-address' => '0.0.0.0:9427',
  'web.telemetry-path' => '/metrics'
}

# Systemd service unit
default[cookbook_name]['unit'] = {
  'Unit' => {
    'Description' => 'Ping exporter service',
    'After' => 'network.target'
  },
  'Service' => {
    'Type' => 'simple',
    'User' => node[cookbook_name]['user'],
    'Group' => node[cookbook_name]['group'],
    'Restart' => 'on-failure',
    'ExecStart' => 'TO BE COMPLETED'
  },
  'Install' => {
    'WantedBy' => 'multi-user.target'
  }
}

# If ping exporter service is restarted when config file changes
default[cookbook_name]['auto_restart'] = true
