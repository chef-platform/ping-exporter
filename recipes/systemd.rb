# Copyright (c) 2017 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Construct systemd unit content with command line options
cli_opts = node[cookbook_name]['cli_opts'].map do |k, v|
  "#{' ' * 2}-#{k} #{v}"
end.join(" \\\n")

prefix_home = node[cookbook_name]['prefix_home']
unit = node[cookbook_name]['unit'].to_h
unit['Service']['ExecStart'] =
  "#{prefix_home}/ping_exporter/ping_exporter \\\n#{cli_opts}"

# Systemd service unit
systemd_unit 'ping-exporter.service' do
  content unit
  action %i[create enable start]
end
