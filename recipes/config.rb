# Copyright (c) 2017 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

::Chef::Recipe.send(:include, ClusterSearch)

# Deploy ping exporter configuration
header = '# Produced by Chef -- changes will be overwritten'
auto_restart = node[cookbook_name]['auto_restart']

prefix_home = node[cookbook_name]['prefix_home']
config_file = node[cookbook_name]['config_file']

targets = cluster_search(node[cookbook_name]['targets'])
config = { 'targets' => targets['hosts'] }

file "#{prefix_home}/ping_exporter/#{config_file}" do
  content "#{header}\n#{config.to_yaml}"
  owner node[cookbook_name]['user']
  group node[cookbook_name]['group']
  mode '0644'
  notifies :restart, 'systemd_unit[ping-exporter.service]' if auto_restart
end
