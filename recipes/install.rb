#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe 'ark'

# Create prefix directories
[
  node[cookbook_name]['prefix_root'],
  node[cookbook_name]['prefix_home'],
  node[cookbook_name]['prefix_bin']
].uniq.each do |dir_path|
  directory "ping-exporter:#{dir_path}" do
    path dir_path
    owner 'root'
    group 'root'
    mode '0755'
    recursive true
    action :create
  end
end

# Download and install ping-exporter
ark 'ping_exporter' do
  action :install
  url node[cookbook_name]['mirror']
  extension 'zip'
  prefix_root node[cookbook_name]['prefix_root']
  prefix_home node[cookbook_name]['prefix_home']
  prefix_bin node[cookbook_name]['prefix_bin']
  has_binaries ['ping_exporter']
  checksum node[cookbook_name]['checksum']
  version node[cookbook_name]['version']
  strip_components 0
end

# Add cap_net_raw to ping and ping-exporter binary needed if user is not root
unless node[cookbook_name]['user'].eql? 'root'
  [
    Mixlib::ShellOut.new('which ping').run_command.stdout.chomp,
    "#{node[cookbook_name]['prefix_home']}/ping_exporter/ping_exporter"
  ].each do |binary|
    execute "add cap_net_raw to #{binary}" do
      command "setcap cap_net_raw+ep #{binary}"
      not_if "getcap #{binary} | grep -q cap_net_raw+ep"
    end
  end
end
